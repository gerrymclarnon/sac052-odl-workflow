(function($) {
    'use strict';
    uoe.forms = {};

    /*
    * VARIABLES
    */
    var validatorObj; // Validator object
    var validateErrorClass = 'uoe-validate-error';
    var validateOkClass = 'uoe-validate-ok';
    var validatorInitialised = false;

    function _enableConditional(attribute, action) {
        function calculateRequirements() {
            $('[' + attribute + ']').each(function(index, value) {
                action($(value), $(value).isMatched(attribute));
            });
        }

        $.fn.isMatched = function(attribute) {
            var $this = $(this).attr(attribute) ? $(this) : $(this).closest('[' + attribute + ']');
            var rules = $this.attr(attribute);

            // Does it even have a conditional-display attribute?
            if (!$this.length || !rules && $this.parents('[' + attribute + ']').length === 0) {
                return true;
            }

            // Get the conditions
            var conditions = rules.split(/([&|()])/g);

            // Check each condition into the string
            for (var i = 0; i < conditions.length; i++) {
                if (conditions[i] === '&' || conditions[i] === '|' || conditions[i] === '(' || conditions[i] === ')' || !conditions[i]) {
                    continue;
                }

                // Break up the string based on suitable conditions
                // Using Array.pop() below returns the last value and takes it off the underlying array
                // So a=[1,2,3];a.pop(); returns 3 and leaves a==[1,2]
                var condition = conditions[i].split(/(=|!=|>=?|<=?)/g);
                var expectedVal = condition.pop();
                var operator = condition.pop();
                var $obj = $(condition.join('')).filter(':input');
                var val = $obj.attr('type') === 'radio' ? $obj.filter(':checked').val() : $obj.val();

                if (typeof val === 'undefined') return false;

                // Number conversion
                var iVal = parseInt(val, 10);
                var iExpectedVal = parseInt(expectedVal, 10);

                // Depending on the operator, calculate whether the condition was met
                if (operator === '=') {
                    conditions[i] = (val.toString() === expectedVal);
                } else if (operator === '!=') {
                    conditions[i] = (val.toString() !== expectedVal);
                } else if (operator === '>') {
                    conditions[i] = (iVal > iExpectedVal);
                } else if (operator === '<') {
                    conditions[i] = (iVal < iExpectedVal);
                } else if (operator === '>=') {
                    conditions[i] = (iVal >= iExpectedVal);
                } else if (operator === '<=') {
                    conditions[i] = (iVal <= iExpectedVal);
                } else {
                    conditions[i] = false;
                }
            }
            // Evaluate the new condition, which is of the form "true&(false|true)"
            // Using eval is okay here, since we can guarantee that the string is just made up of |, &, (, ), true and false.
            var match = conditions.join('');
            var ret = !!eval(match); // jshint ignore:line
            return ret;
        }

         // Attach the functionality to all input elements
        $('body').on('change keyup focus unfocus', ':input', calculateRequirements);
        calculateRequirements();
    }

    function _enableConditionalDisplay() {
        _enableConditional('data-uoe-conditional-display', function($obj, isMatched) { $obj.toggleClass('hidden', !isMatched); });
    }

    function _enableConditionalDisable() {
        _enableConditional('data-uoe-conditional-disable', function($obj, isMatched) {
            if (!isMatched) {
                $obj.removeAttr('disabled');
            } else {
                $obj.attr('disabled', 'disabled');
            }
        });
    }

    uoe.forms.enableConditionalDisplay = _enableConditionalDisplay;
    uoe.forms.enableConditionalDisable = _enableConditionalDisable;

    function enableInputDatePolyfill() {
        uoe.require(['datePolyfill'], function() {
            return true;
        });
    }

    uoe.forms.enableInputDatePolyfill = enableInputDatePolyfill;

    // Transform [data-uoe-for->data-uoe] to [for->id]
    function enableDataFor(context) {
        $('[data-uoe-for]', context).each(function() {
            var $label = $(this);
            var attr = $label.attr('data-uoe-for');
            var lookup = attr.match(/^[a-z0-9-_]+$/i) ? '[data-uoe=' + attr + '],[data-uoe-id=' + attr + ']' : attr;
            var $target = $(lookup);

            if ($target.length) {
                // Determine the labels to use
                var targetId = $target.attr('id');
                var labelId = targetId + 'UOELabel';

                // Delete any hidden labels added by SITS
                $('[for="' + targetId + '"].ui-helper-hidden-accessible').remove();
                // Assign this a proper "for" attribute
                $label.attr({for: targetId, id: labelId});
                // Mark this as the primary label
                $target.attr('aria-labelledby', labelId);
            }
        });
    }

    uoe.forms.enableDataFor = enableDataFor;

    // Allow people to use Ctrl+<up> and Ctrl+<down> to navigate between form fields
    function enableCtrlDirNav(context) {
        $(':input', context).on('keydown', function(e) {
            var $list = $('body').find(':input:visible');
            var index = $list.index(this);

            if (e.ctrlKey && (e.which === 38 || e.which === 40)) {
                $list.eq((index + (e.which === 40 ? 1 : -1)) % $list.length).focus();
                e.preventDefault();
            }
        });
    }

    uoe.forms.enableCtrlDirNav = enableCtrlDirNav;

    /*
    * Form validation
    */

    // Changing types
    function changeTypes() {
        var supportedTypes = ['email', 'number', 'url'];
        $('[data-change-type]').each(function(index) {
            var inputType = $($('[data-change-type]')[index]).attr('data-change-type');
            if ($.inArray(inputType, supportedTypes) > -1) {
                $($('[data-change-type]')[index]).attr('type', inputType);
            } else {
                console.log('Unsupported format for changing type: ' + inputType);
            }
        });
    }

    // Detects any data-uoe-validate-action inputs
    // Modifies the form appropriately
    function setupCancelButtons() {
        if ($('input[type="submit"][data-uoe-validate-action]').length) {
            if ($('input[type="submit"][data-uoe-validate-action="submit"]').length) {
                // Add cancel class to all those that don't have submit
                $('input[type="submit"]:not([data-uoe-validate-action="submit"])').addClass('cancel');
            }
            // Add cancel class to any just labelled cancel.
            $('input[type="submit"][data-uoe-validate-action="cancel"]').addClass('cancel');
        }
    }

    // Function to store the additional methods we have added in.
    function setupCustomMethods(methods) {
        /*
        ** HELPERS
        */
        // Function used in the below date-related methods.
        function dateStringConvert(value) {
            // if it's YYYY-MM-DD or YYYY/MM/DD just parse it
            var patt = new RegExp('^[0-9]{4}[/|-][0-9]{2}[/|-][0-9]{2}$');
            var dateArray;
            if (patt.test(value)) {
                return new Date(value);
            }
            // If it's DD-MM-YYYY or DD/MM/YYYY parse
            patt = new RegExp('^[0-9]{2}[/|-][0-9]{2}[/|-][0-9]{4}$');
            if (patt.test(value)) {
                dateArray = value.split('/');
                if (dateArray.length === 1) {
                    dateArray = value.split('-');
                }

                return new Date(dateArray[2], dateArray[1], dateArray[0]);
            }
            // If it's got a text month ala SITS...
            patt = new RegExp('^[0-9]{2}[/|-][a-zA-Z]{3}[/|-][0-9]{4}$');
            if (patt.test(value)) {
                dateArray = value.split('/');
                if (dateArray.length === 1) {
                    dateArray = value.split('-');
                }
                // Grab the month number
                switch (dateArray[1].toUpperCase()) {
                    case 'JAN':
                        dateArray[1] = 0;
                        break;
                    case 'FEB':
                        dateArray[1] = 1;
                        break;
                    case 'MAR':
                        dateArray[1] = 2;
                        break;
                    case 'APR':
                        dateArray[1] = 3;
                        break;
                    case 'MAY':
                        dateArray[1] = 4;
                        break;
                    case 'JUN':
                        dateArray[1] = 5;
                        break;
                    case 'JUL':
                        dateArray[1] = 6;
                        break;
                    case 'AUG':
                        dateArray[1] = 7;
                        break;
                    case 'SEP':
                        dateArray[1] = 8;
                        break;
                    case 'OCT':
                        dateArray[1] = 9;
                        break;
                    case 'NOV':
                        dateArray[1] = 10;
                        break;
                    case 'DEC':
                        dateArray[1] = 11;
                        break;
                }
                return new Date(dateArray[2],dateArray[1],dateArray[0]);
            }
            // If it's just numbers parse as YYYYMMDD
            patt = new RegExp('^[0-9]{8}$');
            if (patt.test(value)) {
                return new Date(value.substr(0, 4), value.substr(4, 2), value.substr(6, 2));
            }
            // Final case is DD-MM-YY or DD/MM/YY
            patt = new RegExp('^[0-9]{2}[/|-][0-9]{2}[/|-][0-9]{2}$');
            if (patt.test(value)) {
                dateArray = value.split('/');
                if (dateArray.length === 1) {
                    dateArray = value.split('-');
                }

                return new Date('20' + dateArray[2].toString(),dateArray[1],dateArray[0]);
            }

            return false;
        }

        /*
        ** STRING METHODS
        */
        if ($.inArray('string', methods) > -1 || $.inArray('all', methods) > -1) {
            /*
            ** uoe-pattern
            ** Runs regex expressions directly
            */
            uoe.$.validator.addMethod('uoe-pattern', function(value, element) {
                var patt = new RegExp($(element).attr('data-rule-uoe-pattern'));
                return this.optional(element) || patt.test(value);
            }, 'Please enter a valid value.');

            /*
            ** uoe-not-in-list
            ** Checks to see that the value is not in a list of not allowed values (case insensitive).
            */
            uoe.$.validator.addMethod('uoe-not-in-list', function(value, element, params) {
                var thisValue = value.trim().toUpperCase();
                return this.optional(element) || $.inArray(thisValue, params) === -1;
            }, 'This value is not allowed.');
        }

        /*
        ** DATETIME METHODS
        */
        if ($.inArray('datetime', methods) > -1 || $.inArray('all', methods) > -1) {
            /*
            ** uoe-date-between
            ** Verifies a date is between two comma separated dates of same format
            */
            uoe.$.validator.addMethod('uoe-date-between', function(value, element, params) {
                var dateVal = dateStringConvert(value);
                var boundary1 = dateStringConvert(params.split(',')[0]);
                var boundary2 = dateStringConvert(params.split(',')[1]);
                if (boundary2 < boundary1) {
                    // Swap them
                    var tempBoundary = boundary2;
                    boundary2 = boundary1;
                    boundary1 = tempBoundary;
                }

                return this.optional(element) || !(dateVal < boundary1 || dateVal > boundary2);
            }, 'You must enter a valid date.');
            /*
            ** uoe-date-not-future
            ** Verifies a date is not set in the future
            */
            uoe.$.validator.addMethod('uoe-date-not-future', function(value, element) {
                // Grab the date in the right format
                var dateVal = dateStringConvert(value);
                var today = new Date();
                return this.optional(element) || dateVal <= today;
            }, 'You cannot enter a date that is in the future.');
            /*
            ** uoe-date-not-past
            ** Verifies a date is not set in the past
            */
            uoe.$.validator.addMethod('uoe-date-not-past', function(value, element) {
                // Grab the date in the right format
                var dateVal = dateStringConvert($(element).val());
                var today = new Date();
                return this.optional(element) || dateVal >= today;
            }, 'You cannot enter a date that is in the past.');
            /*
            ** uoe-date-consecutive
            ** Verifies two entered dates run in sequence
            */
            uoe.$.validator.addMethod('uoe-date-consecutive', function(value, element, params) {
                var date1 = dateStringConvert(value);
                var date2 = dateStringConvert(params.val());
                return this.optional(element) || (date1 > date2);
            }, 'End date must be after start date.');
            /*
            ** uoe-actual-date
            ** Verifies the date actually exist, e.g. isn't the 30th February or 12th Octember and other examples.
            */
            uoe.$.validator.addMethod('uoe-actual-date', function(value, element) {
                var dateVal = {};
                // Pattern match it
                var patt = new RegExp('^[0-9]{4}[/|-][0-9]{2}[/|-][0-9]{2}$');
                if (patt.test(value)) {
                    dateVal.year = value.substr(0, 4);
                    dateVal.month = parseInt(value.substr(5, 2)) - 1;
                    dateVal.day = parseInt(value.substr(8, 2));
                } else {
                    // Not a properly entered date yet
                    // Return that it's OK, this validation done by another function...
                    return true;
                }
                // Setup the array of days in each month
                var days = []; days['0'] = 31; days['1'] = 28; days['2'] = 31; days['3'] = 30; days['4'] = 31; days['5'] = 30; days['6'] = 31; days['7'] = 31;
                days['8'] = 30; days['9'] = 31; days['10'] = 30; days['11'] = 31;
                // Is it a leap year?
                if (dateVal.year % 4 === 0) {
                    days['1'] = 29;
                }
                // Check the dates
                return this.optional(element) || dateVal.day <= days[dateVal.month];
            }, 'The day you have selected does not exist.');
            /*
            ** uoe-valid-date
            ** Verifies that the supplied date is valid both in format and existence
            ** Automatically formats the date to "DD/MON/YYY", as that's how SITS
            ** expects it
            */
            uoe.$.validator.addMethod('uoe-valid-date', function(value, element) {
                var dateVal = {};

                // If there's no value, there's nothing to validate. If the field
                // is required, that will separately be handled by another rule.
                if (!value) return true;

                // Create a Regex pattern matcher
                var patt = /^\d{1,2}(?:st|nd|th)?(\s+|[/-])(?:[A-Z]{3,}|\d{1,2})\1\d{4}$/i;

                // Define some facts about the months
                var months = {
                    1: {days: 31, name: 'January'},
                    2: {days: 28, name: 'February'},
                    3: {days: 31, name: 'March'},
                    4: {days: 30, name: 'April'},
                    5: {days: 31, name: 'May'},
                    6: {days: 30, name: 'June'},
                    7: {days: 31, name: 'July'},
                    8: {days: 31, name: 'August'},
                    9: {days: 30, name: 'September'},
                    10: {days: 31, name: 'October'},
                    11: {days: 30, name: 'November'},
                    12: {days: 31, name: 'December'}
                };

                // Confirm date is in a valid format
                if (!patt.test(value)) {
                    return false;
                }

                // Split the date into day, month and year
                value = value.split(/\s+|[/-]{1}/);

                // Check year is 4 digits
                if (value[2].length !== 4) {
                    return false;
                }

                // Parse the day and year
                dateVal.day = parseInt(value[0], 10);
                dateVal.year = parseInt(value[2], 10);

                // Convert the month to an integer
                if (!isNaN(parseInt(value[1], 10))) {
                    // Month is already a number
                    dateVal.month = parseInt(value[1], 10);
                } else {
                    // Month is text, try and match it to a name
                    var monthName = value[1].toUpperCase();
                    for (var i in months) {
                        var toMatch = months[i].name.toUpperCase();
                        if (monthName === toMatch || monthName === toMatch.substr(0, monthName.length)) {
                            dateVal.month = i;
                            break;
                        }
                    }
                }

                // If month wasn't determined, the format isn't right
                if (typeof dateVal.month === 'undefined') {
                    return false;
                }

                // Adapt the days in February if a leap year
                if (dateVal.year % 4 === 0) {
                    months['2'].days = 29;
                }

                // If the date is correct, reformat to datepicker format and return true
                if (this.optional(element) || (typeof months[dateVal.month] === 'object' && dateVal.day <= months[dateVal.month].days)) {
                    // Format day as 2 digits for output
                    if (dateVal.day < 10) dateVal.day = '0' + dateVal.day;

                    // Set the format in the element
                    element.value = dateVal.day + '/' + months[dateVal.month].name.substr(0, 3) + '/' + dateVal.year;
                    return true;
                } else {
                    // Otherwise return false
                    return false;
                }
            }, 'Please enter in the format dd/mon/yyyy (e.g. 21/Jul/1969)');
        }
    }

    /*
    ** Given a selector statement, quickly grab the input name.
    */
    function getNameFromSel(sel) {
        var $el = $(sel);
        if (typeof $el.attr('name') === 'undefined') {
            $el.attr('name', uoe.guid());
        }

        return ($el).attr('name');
    }

    /*
    ** Takes one of the arrays to be inputted to the validator
    ** Converts the selector given as keys into names
    */
    function selectorKeysToNames(arr) {
        var output = {};
        for (var sel in arr) {
            output[ getNameFromSel(sel) ] = arr[sel];
        }

        return output;
    }

    /*
    ** Resets the form to its pre-validation state and removes error class from inputs
    */
    function resetValidation() {
        validatorObj.resetForm();
        $('.' + validateErrorClass).removeClass(validateErrorClass);
    }

    function loadValidation(rules, options, callback) {
        var params = options ? { formSelector: options.formSelector || '', customMethods: options.customMethods || ['all']} : {};

        // Load in the plugin
        uoe.require(['pubsub', 'validate-plugin'], function() {
            if (!validatorInitialised) {
                uoe.$.validator.setDefaults({
                    errorClass: validateErrorClass,
                    validClass: validateOkClass,
                    ignoreTitle: true,
                    errorPlacement: function($error, $element) {

                        var $el = $element.closest('fieldset');
                        if ($el.length) {
                            $error.insertAfter($el);
                            return true;
                        }

                        $el = $element.closest('.input-group');
                        if ($el.length) {
                            $error.insertAfter($el);
                            return true;
                        }

                        $el = $element.closest('.uoe-dynamic-listbox');
                        if ($el.length) {
                            $error.insertAfter($el);
                            return true;
                        }

                        if ($element.next().hasClass('ui-datepicker-trigger')) {
                            $error.insertAfter($element.next());
                            return true;
                        }

                        $el = $element.closest('.checkbox-inline');
                        if ($el.length) {
                            $error.insertAfter($el.parent().find('div:last-child'));
                            return true;
                        }

                        $el = $element.closest('.form-group');
                        if ($el.length) {
                            $error.appendTo($el);
                            return true;
                        }

                        $error.insertAfter($element);
                    }
                });

                changeTypes();
                setupCancelButtons();

                validatorInitialised = true;
            }

            if (params.customMethods) {
                setupCustomMethods(params.customMethods);
            } else {
                setupCustomMethods(['all']);
            }

            if (callback) {
                return callback();
            }

            $.publish('uoe_forms/validator/loaded', 'loaded'); // publish loaded event
        });
    }

    // Run the validation
    function validateForm(rules, options) {
        loadValidation(rules, options, function() {
            var params = options ? { formSelector: options.formSelector || '', customMethods: options.customMethods || ['all']} : {};
            validatorObj = params.formSelector ? $(params.formSelector).validate(rules) : $($('form')[0]).validate(rules); // initialise validation (use selector if given)
            $.publish('uoe_forms/validator/ready', 'ready'); //publish ready event

            return validatorObj;
        });
    }

    uoe.forms.loadValidation = loadValidation;
    uoe.forms.validator = validatorObj; // Exposes the validator object
    uoe.forms.resetValidation = resetValidation;
    uoe.forms.validateForm = validateForm;
    uoe.forms.getNameFromSel = getNameFromSel;
    uoe.forms.selectorKeysToNames = selectorKeysToNames;

})(uoe.$);
