html5shiv.min.js: The HTML5 Shiv

The HTML5 Shiv enables use of HTML5 sectioning elements in legacy 
Internet Explorer and provides basic HTML5 styling for Internet 
Explorer 6-9, Safari 4.x (and iPhone 3.x), and Firefox 3.x.

Project homepage: https://code.google.com/p/html5shiv/


uoe.js: Gel-specific javascript

Javascript needed to support responsive features of the Gel.
