//This file is here to give some basic functionality to the prototype interface
//Clean this up and replace with proper structure in the future

$(function () {
  var getCollapseControl = function($collapsible) {
    return $('.btn-details[data-target="#' + $collapsible.attr('id') + '"]');
  };

  $('.app-details').on('hide.bs.collapse', function () {
    getCollapseControl($(this)).html('<i class="fa fa-caret-right"></i> Show details');
  });
  $('.app-details').on('show.bs.collapse', function () {
    getCollapseControl($(this)).html('<i class="fa fa-caret-down"></i> Hide details');
  });

  $('[data-toggle="tooltip"]').tooltip({placement: 'right'});
});