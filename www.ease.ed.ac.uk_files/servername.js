function setServer() {

	var server;

	if (location.host == "www-dev.ease.ed.ac.uk") {
		server="DEV EASE";
	} else if (location.host == "www-test.ease.ed.ac.uk") {
		server="TEST EASE";
	} else {
		server="EASE";
	}

	if (document.getElementById("server")) {
		var current = document.getElementById("server").innerHTML;
		document.getElementById("server").innerHTML = current.replace("EASE", server);
	}
	var slice = document.getElementById("loaf").innerHTML;
	document.getElementById("loaf").innerHTML = slice.replace("EASE", server);
}
