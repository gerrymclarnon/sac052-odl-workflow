// We wrap the whole page in an immediately-invoked function expression, which scopes functions and variables
// Note that in this we also define $ to locally represent uoe.$
// Contributors: PH, TP
// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
(function($) {

    // Set the function to "strict mode", which forces you to write better code.
    // For example, it won't let you access variables until they have been defined with var (which stops global leaking)
    'use strict';

    // Create our namespace for this project
    uoe.apphub = {};

   /**
    * FUNCTIONS
    */

    function processing() {
        $('.bg').fadeIn();
        $('body').addClass('processing');
    }

    function complete() {
        $('.bg').fadeOut();
        $('body').removeClass('processing');
    }

    function scrollTo($el) {
        $('html, body').stop().animate({
            scrollTop: $el.offset().top
        }, 300, 'swing', function() {
            // On complete?
        });
    }

    //function to generate PDF file on the server
    function generatePdf() {
        processing();
        //create the url to the SRL.APPHUB_PDF
        var domain = $('#domain').html();
        var url = domain + $('#link').html().slice(7);
        //do the ajax call to the SRL.APPHUB_PDF to generate pdf on the server
        $.ajax({
            url: url,
            success: function() {
                //if success open the pdf download prompt but wait 4s the file will be fully generated
                setTimeout(function() {
                    var href = domain + $('#downloadPdf').html().slice(7);
                    complete();
                    window.location.href = href;
                }, 4000);

            },

            error: function(e) {
            console.log(e.message);
            }
        });
    }

     // To make a function public, we must add it to our namespace
     //uoe.apphub.publicFunction = publicFunction;

    uoe.$(document).ready(function()  {
    switch (uoe.$('span[data-page-id]').attr('data-page-id')) {
        /* USED in SRL APPHUB_APP.CAP */
        case 'apphub-app':
            $('[data-uoe-scroll-to]').click(function() {
                $('ul.nav-tabs li a[href="' + $(this).attr('data-uoe-scroll-to') + '"]').click();
                scrollTo($('[data-uoeid="application-tab-content"]'));
                return true;
            });

            break;
        /* Used in TTE APPHUB_DOC.STEP1 */
        case 'documents-upload':
		
			//hide SITS next button and use the TTQ button to click it
			$('input[value="Next"]').hide();
			$('*[data-uoeid="UploadBtn"]').click(function(){$('input[value="Next"]').click();});
			
            var size = $('#file-size').html();
            uoe.forms.validateForm({
                rules: uoe.forms.selectorKeysToNames({
                    '[data-uoeid="DOC-UPLOAD"]': {
                    required: true,
                    extension: 'doc|docx|rtf|pdf|txt|jpg|gif|png|xls|xlsx|msg',
                    filesize: size
                    }
                })
            });
            break;
        case 'offer-select':
        /* Used in TTE APPHUB_CAP.OFFER_STEP1 AND APPHUB_CAP.CAS_STEP1 */
            uoe.forms.validateForm({
                //specify the error placement for radio button
                errorPlacement: function(error) {
                    error.appendTo($('#error-placement'));
                }
            });
            break;
        case 'offer-questionnaire':
            /* Used in TTE APPHUB_CAP.OFFER_STEP4 */

            $('[data-uoeid="decl-res"]').change(function() {

                    var dec = $('[data-uoeid="decl-res"]').val();
                    var offerlbl = 'Reason 1';
                    var otherlbl = 'Other reasons for withdrawal';

                    if (dec === 'OFFERELSEWHERE') {
                        $('[data-uoeid="other-ins"]').prop('required', true);
                        $('#offer-lbl').html(offerlbl + ' *');
                        $('[data-uoeid="other-res"]').prop('required', false);
                        $('#other-lbl').html(otherlbl);
                    } else if (dec === 'OTHER') {
                        $('[data-uoeid="other-ins"]').prop('required', false);
                        $('#offer-lbl').html(offerlbl);
                        $('[data-uoeid="other-res"]').prop('required', true);
                        $('#other-lbl').html(otherlbl + ' *');
                    } else {
                        $('[data-uoeid="other-ins"]').prop('required', false);
                        $('#offer-lbl').html(offerlbl);
                        $('[data-uoeid="other-res"]').prop('required', false);
                        $('#other-lbl').html(otherlbl);
                    }
            });

            uoe.forms.validateForm();
            break;
        case 'edit-referee-details':
            /* Used in TTE APPHUB_CAP.REF_STEP2 */
            uoe.forms.validateForm({
                submitHandler: function(form) {
                    processing();
                    form.submit();
                }
            });
            break;
        case 'edit-personal-details':
            /* Used in TTE APPHUB_STU.PERSONAL */

            // change the months from the mm format to the mmm format (TTQ.Dropdown Dates)
            $('[data-uoeid="stu_dob"]select:eq(1) > option').each(function() {
            switch (this.value) {
                        case '01':
                            $(this).html('January');
                            break;
                        case '02':
                            $(this).html('February');
                            break;
                        case '03':
                            $(this).html('March');
                            break;
                        case '04':
                            $(this).html('April');
                            break;
                        case '05':
                            $(this).html('May');
                            break;
                        case '06':
                            $(this).html('June');
                            break;
                        case '07':
                            $(this).html('July');
                            break;
                        case '08':
                            $(this).html('August');
                            break;
                        case '09':
                            $(this).html('September');
                            break;
                        case '10':
                            $(this).html('October');
                            break;
                        case '11':
                            $(this).html('November');
                            break;
                        case '12':
                            $(this).html('December');
                            break;
                        //case "":
                            //$(this).remove();
                        //    break;
                }
            });
            //restrict the year range (TTQ.Dropdown Dates)
            var year = new Date().getFullYear();
            $('[data-uoeid="stu_dob"]select:eq(2) > option').each(function() {
                if ((this.value > year - 10 || this.value < year - 100) && this.value !== '') {
                    $(this).remove();
                }
            });
            //remove blank select from Day dropdown (TTQ.Dropdown Dates)
            //$("[data-uoeid='stu_dob']select:eq(0) > option").each(function() {
            //    if(this.value === "") {
                //    $(this).remove();
                //}
            //});

            uoe.forms.validateForm({
                submitHandler: function(form) {
                    processing();
                    form.submit();
                }
            });
            // Toggling address fields
            $('input[data-input-toggle-cat]').click(function() {
                var cat = $(this).attr('data-input-toggle-cat');
                var v = $(this).val();
                // Clear existing input data
                $('[data-input-cat="' + cat + '"]').val('');
                $('[data-input-cat="' + cat + '"]').removeAttr('required');
                // Display the correct inputs
                $('[data-input-cat="' + cat + '"]').hide();
                $('[data-input-cat="' + cat + '"][data-address-cat="' + v + '"]').show();
                if (v === 'U' || v === 'F') {
                    $('[data-input-cat="' + cat + '"][data-address-cat="F|U"]').show();
                    $('[data-input-cat="' + cat + '"][data-address-cat="U|F"]').show();
                }
                // Update to the correct placeholders
                $('[data-input-cat="' + cat + '"][data-placeholder-' + v.toLowerCase() + ']').each(function(index) {
                    var holder = $($('[data-input-cat="' + cat + '"][data-placeholder-' + v.toLowerCase() + ']')[index]).attr('data-placeholder-' + v.toLowerCase());
                    $($('[data-input-cat="' + cat + '"][data-placeholder-' + v.toLowerCase() + ']')[index]).attr('placeholder', holder);
                });

                // Update the correct requirement criteria
                $('[data-input-cat="' + cat + '"][data-set-required]:visible').prop('required', true);
                // Hide previous error messages
                $('[data-input-cat="' + cat + '"]').removeClass('error');
                $('[data-input-cat="' + cat + '"] + label.error').hide();
            });
			
			$('[data-input-toggle-cat]:checked').click();

            break;
        case 'mhd-reply':
            // Used in TTE APPHUB_MHD.REPLY
            uoe.forms.validateForm();
            break;
        case 'change-req':
            // Used in TTE APPHUB_CAP.CHANGEREQ_1
            $('input[type="checkbox"][data-toggle]').click(function() {
                if ($(this).is(':checked')) {
                    $('[data-panel="' + $(this).attr('data-toggle') + '"]').slideDown();
                    // Reenable the inputs
                    $('[data-panel="' + $(this).attr('data-toggle') + '"] *').removeAttr('disabled');
                } else {
                    $('[data-panel="' + $(this).attr('data-toggle') + '"]').slideUp();
                    // Disable the inputs
                    $('[data-panel="' + $(this).attr('data-toggle') + '"] *').attr('disabled', 'disabled');
                }
            });

            var rules = {
                '[data-uoeid="CHANGE_YOE"]':{
                    require_from_group: [1, '[data-input-cat]']
                },
                '[data-uoeid="CHANGE_SD"]':{
                    require_from_group: [1, '[data-input-cat]']
                },
                '[data-uoeid="CHANGE_PROG"]':{
                    require_from_group: [1, '[data-input-cat]']
                }
            }

            var messages = {
                '[data-uoeid="CHANGE_YOE"]':{
                    require_from_group: 'Please select at least one field to change'
                },
                '[data-uoeid="CHANGE_SD"]':{
                    require_from_group: 'Please select at least one field to change'
                },
                '[data-uoeid="CHANGE_PROG"]':{
                    require_from_group: 'Please select at least one field to change'
                }
            }

            uoe.forms.validateForm({
                rules: uoe.forms.selectorKeysToNames(rules),
                messages: uoe.forms.selectorKeysToNames(messages),
                errorPlacement: function(error, element) {
                    if (element.attr('type') === 'checkbox') {
                      error.insertAfter('label[for="' + element.attr('id') + '"][data-custom-label]');
                    } else {
                      error.insertAfter(element);
                    }
                },

                submitHandler: function(form) {
                    processing();
                    form.submit();
                }
            });

            break;
        case 'FEESTATUS':
            // Used in TTE FSQFORM.FEESTATUS
            $('input[value="Next"]').hide();
            $('*[data-uoeid="ContinueBtn"]').click(function() {
                $('input[value="Next"]').click();
            });

            uoe.forms.validateForm({
                errorLabelContainer: '#mandatory'
            });

            break;
        case 'FSQVALIDATION':
            // Used in multiple TTEs FSQFORM to apply the validation
            uoe.forms.validateForm();
            break;
        case 'FSQVALIDATIONRADIO':
            // Used in TTE FSQFORM.FEESTATUS
            uoe.forms.validateForm({
                errorLabelContainer: '#mandatory'
            });

            break;
        case 'FSQUPLOAD':
            // Used in TTE FSQFORM.FEESTATUS
            $('input[value="Upload"]').hide();
            $('*[data-uoeid="UploadBtn"]').click(function() {
                $('input[value="Upload"]').click();
            });

            uoe.forms.validateForm();
            break;
        case 'FSQNEXT':
            // Used in TTE FSQFORM.FEESTATUS
            $('input[value="Next"]').hide();
            $('*[data-uoeid="ContinueBtn"]').click(function() {
                $('input[value="Next"]').click();
            });

            break;
        /* Used in TTE APPHUB_DOC.STEP1 */
        case 'documents-upload':
            //hide SITS next button and use the TTQ button to click it
            $('input[value="Next"]').hide();
            $('*[data-uoeid="UploadBtn"]').click(function() {
                $('input[value="Next"]').click();
            });

            break;
        /* Used in SRL.APPHUB_PDF */
        case 'pdf-generate':
            $('#generatePdf').click(function() {
                generatePdf();
            });

            break;
    }
    });

})(uoe.$);
