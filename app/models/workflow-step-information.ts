export class WorkflowStepInformation {
  title: string;
  url: string;
  supportText: string;
}
