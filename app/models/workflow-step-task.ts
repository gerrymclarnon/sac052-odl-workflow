import {WorkflowStepInformation} from "./workflow-step-information";

export class WorkflowStepTask extends WorkflowStepInformation {
  status: string;
}
