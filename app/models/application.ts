import {WorkflowStep} from './workflow-step';
import {WorkflowStepTask} from "./workflow-step-task";
import {WorkflowStepInformation} from "./workflow-step-information";

export class Application {
  stage: string;
  status: string;
  programme: string;
  entryYear: string;
  workflow: WorkflowStep[];
  tasks: WorkflowStepTask[];
  information: WorkflowStepInformation[];
  links: WorkflowStepInformation[];
}
