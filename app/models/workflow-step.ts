import {WorkflowStepTask} from "./workflow-step-task";
export class WorkflowStep {
  title: string;
  status: string;
  tooltip: string;
  supportText: string;
}
