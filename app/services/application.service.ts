import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';

import {Application} from './../models/application';

@Injectable()
export class ApplicationService {
  private http:Http;

  constructor (http:Http) {
    this.http = http;
  }

  getApplications(file:string) {
    return this.http.get(file)
      .map((res:Response) => res.json());
  }
}
