import {Component, OnInit, ViewChild} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';

import {Application} from './../../models/application';
import {ApplicationComponent} from '../application/application.component';
import {ApplicationService} from './../../services/application.service';
import {ApplicationsComponent} from "../application/applications.component";
import {TestDataFileSelectComponent} from "../test/test-data-file-select.component";
import {TestWorkflowNavigationComponent} from "../test/test-workflow-navigation.component";

declare var jQuery:any;

@Component({
  templateUrl: 'app/components/home/home.component.html',
  directives: [ApplicationsComponent, TestDataFileSelectComponent, TestWorkflowNavigationComponent],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class HomeComponent implements OnInit {

  @ViewChild(TestWorkflowNavigationComponent)
  private testWorkflowNavigationComponent:TestWorkflowNavigationComponent;

  private _params:any;
  private _applicationService:ApplicationService;
  private _applications:Application[];
  private _newStudent:Application[];
  private dataFile:string;
  private workflowFile:string;
  private _devMode:boolean;
  private _testMode:boolean;
  private _hostname:string;

  constructor(
    applicationService:ApplicationService,
    params:RouteParams) {
    this._applicationService = applicationService;
    this.dataFile = this.getParam('file');
    this.workflowFile = this.getParam('workflow');
    // TODO - remove setting testMode to true if falsey testMode param
    this._devMode = this.getParam('devMode') ? this.getParam('devMode') === "true" : true;
    this._testMode = this.getParam('testMode') ? this.getParam('testMode') === "true" : true;
    this._hostname = `${window.location.origin}/home`;
  }

  ngOnInit() {
    this.getApplications();
  }

  get applications():Application[]{
    return this._applications;
  }

  get newStudent():Application[]{
    return this._newStudent;
  }

  get devMode():boolean{
    return this._devMode;
  }

  get testMode():boolean{
    return this._testMode;
  }

  get hostname():string{
    return this._hostname;
  }

  getParam(name:string):string {
    let paramKeyValues = window.location.search.substring(1, window.location.search.length).split("&");

    if (!this._params) {
      this._params = new Map();
      for (var prop in paramKeyValues) {
        var keyValue = paramKeyValues[prop].split("=");
        this. _params.set(keyValue[0], keyValue[1]);
      }
    }

    return this._params.get(name);
  }

  getApplications() {
    this._applicationService.getApplications(this.dataFile)
      .subscribe(
        data => {
          this._applications = data.applications;
          this._newStudent = data.newStudent;
        },
        err => console.error(err),
        () => console.log(`applications loaded from ${this.dataFile}`)
      );
  }

  onApplicationsChange($event:any){
    this._applications = $event;
  }

  onNewStudentChange($event:any){
    this._newStudent = $event;
  }

  onButtonClick($event:any) {
    console.log("task button clicked");
  }

  onClick($event:any) {
    if (jQuery($event.target).is("a")) {
      if ($event.target.id === "NextWorkflowStep") {
        this.testWorkflowNavigationComponent.nextWorkflowStep($event);
      } else if ($event.target.id) {
        window.location = $event.target.id;
      }
    }
  }
}
