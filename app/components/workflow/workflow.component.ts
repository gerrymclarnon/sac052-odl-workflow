import {Component, Input, OnInit} from '@angular/core';

import {WorkflowStep} from './../../models/workflow-step';
import {WorkflowStepComponent} from "./workflow-step.component";
import {WorkflowStepTask} from "../../models/workflow-step-task";
import {WorkflowStepInformation} from "../../models/workflow-step-information";

@Component({
  selector: 'workflow',
  templateUrl: 'app/components/workflow/workflow.component.html',
  styleUrls: ['app/components/workflow/workflow.component.css'],
  directives: [WorkflowStepComponent]
})
export class WorkflowComponent implements OnInit {
  @Input()
  workflow: WorkflowStep[];

  @Input()
  tasks: WorkflowStepTask[];

  @Input()
  information: WorkflowStepInformation[];

  constructor() {
  }

  ngOnInit() {
  }
}
