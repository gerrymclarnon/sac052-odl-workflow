import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';

import {WorkflowStep} from './../../models/workflow-step';
import {WorkflowStepTasksComponent} from "./workflow-step-tasks.component";
import {WorkflowStepTask} from "../../models/workflow-step-task";
import {WorkflowStepInformation} from "../../models/workflow-step-information";
import {AbstractHelpComponent} from "./abstract-help.component";

declare var jQuery:any;

@Component({
  selector: 'workflow-step',
  templateUrl: 'app/components/workflow/workflow-step.component.html',
  directives: [WorkflowStepTasksComponent]
})
export class WorkflowStepComponent extends AbstractHelpComponent implements OnInit {
  @Input()
  step: WorkflowStep;

  @Input()
  tasks: WorkflowStepTask[];

  @Input()
  information: WorkflowStepInformation[];

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getHelpText():string {
    return this.step.tooltip;
  }
}
