import {Component, Input, OnInit} from '@angular/core';

import {WorkflowStepTask} from './../../models/workflow-step-task';
import {WorkflowStepTaskComponent} from "./workflow-step-task.component";
import {WorkflowStepInformationComponent} from "./workflow-step-information.component";
import {WorkflowStepInformation} from "../../models/workflow-step-information";

@Component({
  selector: 'workflow-step-tasks',
  templateUrl: 'app/components/workflow/workflow-step-tasks.component.html',
  directives: [WorkflowStepTaskComponent, WorkflowStepInformationComponent]
})
export class WorkflowStepTasksComponent implements OnInit {
  @Input()
  tasks: WorkflowStepTask[];

  @Input()
  information: WorkflowStepInformation[];

  constructor() {
  }

  ngOnInit() {
  }

  get hasOpenTasks():boolean {
    for (let task of this.tasks) {
      if (task.status === 'open') {
        return true;
      }
    }
    return false;
  }
}
