import {Component, Input, OnInit} from '@angular/core';

import {WorkflowStepInformation} from "../../models/workflow-step-information";

@Component({
  selector: 'workflow-step-information',
  templateUrl: 'app/components/workflow/workflow-step-information.component.html'
})
export class WorkflowStepInformationComponent implements OnInit {
  @Input()
  information: WorkflowStepInformation;

  constructor() {
  }

  ngOnInit() {
  }
}
