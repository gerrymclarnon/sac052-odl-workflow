import {OnInit} from '@angular/core';


declare var jQuery:any;

export abstract class AbstractHelpComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  abstract getHelpText():string;

  ngAfterViewInit() {
    jQuery('[data-toggle="tooltip"]').tooltip({placement: 'right'});

    jQuery('[data-toggle="popover"]').popover({
      placement: 'right',
      title: 'Information',
      content: this.getHelpText(),
      trigger: 'hover'
    });

    jQuery('[data-toggle="popover"]').on('click', function (e:any) {
      jQuery('[data-toggle="popover"]').not(this).popover('hide');
    });
  }
}
