import {Component, Input, OnInit} from '@angular/core';

import {WorkflowStepTask} from './../../models/workflow-step-task';
import {AbstractHelpComponent} from "./abstract-help.component";

declare var jQuery:any;

@Component({
  selector: 'workflow-step-task',
  templateUrl: 'app/components/workflow/workflow-step-task.component.html'
})
export class WorkflowStepTaskComponent extends AbstractHelpComponent implements OnInit {
  @Input()
  task: WorkflowStepTask;

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getHelpText():string {
    return this.task.supportText;
  }
}
