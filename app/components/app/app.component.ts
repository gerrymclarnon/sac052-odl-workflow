import { Component, OnInit } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';

import { ApplicationService } from './../../services/application.service';
import { ApplicationsComponent } from './../application/applications.component';
import { HomeComponent } from './../home/home.component';

declare var jQuery:any;

@Component({
  selector: 'my-app',
  templateUrl: 'app/components/app/app.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [
    ROUTER_PROVIDERS,
    ApplicationService
  ]
})
@RouteConfig([
  {path: 'home', name: 'Home', component: HomeComponent, useAsDefault: true},
  {path: '**', redirectTo: ['Home']}
])
export class AppComponent implements OnInit {
  public title:string = 'SAC052 ODL Workflow Prototype';

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    jQuery('body').on('click', function (e:any) {
      jQuery('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!jQuery(this).is(e.target)
          && jQuery(this).has(e.target).length === 0
          && jQuery('.popover').has(e.target).length === 0) {
          jQuery(this).popover('hide');
        }
      });
    });
  }
}
