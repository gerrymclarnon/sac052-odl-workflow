import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';

import {ApplicationService} from "../../services/application.service";
import {MockWorkflow} from "../../mocks/models/mock-workflow";
import {MockWorkflowStepTask} from "../../mocks/models/mock-workflow-step-task";
import {MockWorkflowStep} from "../../mocks/models/mock-workflow-step";
import {Application} from "../../models/application";

@Component({
  selector: 'test-workflow-navigation',
  templateUrl: 'app/components/test/test-workflow-navigation.component.html'
})
export class TestWorkflowNavigationComponent implements OnInit {

  @Input()
  workflowFile:string;

  @Output()
  applicationsChanged: EventEmitter<Application[]> = new EventEmitter();

  @Output()
  newStudentChanged: EventEmitter<Application[]> = new EventEmitter();

  private _applicationService:ApplicationService;
  private _workflow:MockWorkflow;
  private _currentWorkflowStepNumber:number = 0;

  constructor(
    applicationService:ApplicationService) {
    this._applicationService = applicationService;
  }

  ngOnInit() {
    this.getWorkflow();
  }

  getWorkflow() {
    this._applicationService.getApplications(this.workflowFile)
      .subscribe(
        data => {
          this._workflow = data;
          this.updateData();
        },
        err => console.error(err),
        () => console.log(`workflow loaded from ${this.workflowFile}`)
      );
  }

  get workflow():MockWorkflow {
    return this._workflow;
  }

  get currentWorkflowStep():MockWorkflowStep {
    try {
      return this._workflow.steps[this._currentWorkflowStepNumber];
    } catch (e) {
      return new MockWorkflowStep();
    }
  }

  get hasPreviousWorkflowStep():boolean {
    return this._currentWorkflowStepNumber !== 0;
  }

  get hasNextWorkflowStep():boolean {
    if (this._workflow && this._workflow.steps) {
      if (!this._workflow.steps[this._currentWorkflowStepNumber].hideNext
      && this._currentWorkflowStepNumber < this._workflow.steps.length - 1) {
        return true;
      }
    }

    return false;
  }

  previousWorkflowStep($event:any) {
    this._currentWorkflowStepNumber--;
    this.updateData();
  }

  nextWorkflowStep($event:any) {
    this._currentWorkflowStepNumber++;
    this.updateData();
  }

  updateData() {
    if (this._workflow.steps[this._currentWorkflowStepNumber].data) {
      let self = this;

      this._applicationService.getApplications(this._workflow.steps[this._currentWorkflowStepNumber].data)
        .subscribe(
          data => {
            self.applicationsChanged.emit(data.applications);
            self.newStudentChanged.emit(data.newStudent);
          },
          err => console.error(err),
          () => console.log(`applications loaded from ${this._workflow.steps[this._currentWorkflowStepNumber].data}`)
        );
    } else {
      this.applicationsChanged.emit(null);
      this.newStudentChanged.emit(null);
    }
  }
}
