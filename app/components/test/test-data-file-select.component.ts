import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';

import {Application} from './../../models/application';
import {ApplicationComponent} from '../application/application.component';
import {ApplicationService} from './../../services/application.service';

@Component({
  selector: 'test-data-file-select',
  templateUrl: 'app/components/test/test-data-file-select.component.html'
})
export class TestDataFileSelectComponent implements OnInit {

  //@Input()
  //file:string;
  //
  @Input()
  applications:Application[];

  @Input()
  newStudent:Application[];

  @Output()
  applicationsChanged: EventEmitter<Application[]> = new EventEmitter();

  @Output()
  newStudentChanged: EventEmitter<Application[]> = new EventEmitter();

  private _hostname:string;

  constructor() {
  }

  ngOnInit() {
  }

  get hostname():string{
    return this._hostname;
  }

  changeListener($event:any): void {
    this.readFile($event.target);
  }

  readFile(inputValue: any) : void {
    let self = this;
    let file:File = inputValue.files[0];
    let myReader:FileReader = new FileReader();

    myReader.onloadend = function(e) {
      let data = JSON.parse(myReader.result);

      console.log(data);

      self.applicationsChanged.emit(data.applications);
      self.newStudentChanged.emit(data.newStudent);
    }

    myReader.readAsText(file);
  }
}
