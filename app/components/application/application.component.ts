import {Component, Input, OnInit} from '@angular/core';

import {Application} from './../../models/application';
import {WorkflowComponent} from "../workflow/workflow.component";

@Component({
  selector: 'application',
  templateUrl: 'app/components/application/application.component.html',
  directives: [WorkflowComponent]
})
export class ApplicationComponent implements OnInit {
  @Input()
  application: Application;

  constructor() {
  }

  ngOnInit() {
  }
}
