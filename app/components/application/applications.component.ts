import {Component, Input, OnInit} from '@angular/core';

import {Application} from './../../models/application';
import {ApplicationComponent} from './application.component';
import {ApplicationService} from './../../services/application.service';
import {WorkflowStepInformationComponent} from "../workflow/workflow-step-information.component";

@Component({
  selector: 'applications',
  templateUrl: 'app/components/application/applications.component.html',
  styleUrls:  ['app/components/application/applications.component.css'],
  directives: [ApplicationComponent, WorkflowStepInformationComponent]
})
export class ApplicationsComponent implements OnInit {

  @Input()
  applications: Application[];

  @Input()
  newStudent: Application[];

  private _applicationService:ApplicationService;

  constructor(
    applicationService:ApplicationService) {
    this._applicationService = applicationService;
  }

  ngOnInit() {
  }
}
