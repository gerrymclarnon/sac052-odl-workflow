import {bootstrap}    from '@angular/platform-browser-dynamic';
import {HTTP_PROVIDERS} from '@angular/http';
import {enableProdMode} from '@angular/core';
import 'rxjs/add/operator/map';

import {AppComponent} from './components/app/app.component';

// TODO - switch on/off depending on config
//enableProdMode();
bootstrap(AppComponent, [
  HTTP_PROVIDERS
]);
