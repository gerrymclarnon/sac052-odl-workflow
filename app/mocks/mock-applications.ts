import { Application } from './../models/application';
export var APPLICATIONS:Application[] = [
  {
    "stage": "Applicant",
    "status": "We are processing your application",
    "programme": "MPH Public Health (Online Distance Learning) - 3 Years",
    "entryYear": "2016/7 entry",
    "workflow": [
      {
        "title": "Application received",
        "status": "complete",
        "tooltip": "We received your application on 1st December 2015.",
        "supportText": ""
      },
      {
        "title": "Process application",
        "status": "process_action",
        "tooltip": "We are processing your application to ensure it is complete",
        "supportText": "We are processing your application to see if we have all of the information needed to make a decision on your application. We will be in touch via email if anything else is needed."
      },
      {
        "title": "All details received",
        "status": "future",
        "tooltip": "We have not yet finished processing your application",
        "supportText": ""
      },
      {
        "title": "Ready for a decision",
        "status": "future",
        "tooltip": "We are not yet ready to amke a decision on your application",
        "supportText": ""
      },
      {
        "title": "Decision",
        "status": "future",
        "tooltip": "We have not yet made a decision on your application",
        "supportText": ""
      }
    ],
    "tasks": [
      {
        "title": "Documentation required",
        "url": "http://www.myed.ed.ac.uk",
        "supportText": "Further documentation is required to support your application and can be uploaded via the applicant hub.",
        "status": ""
      }
    ],
    "information": [
      {
        "title": "TBD",
        "url": "TBD",
        "supportText": "TBD",
      }
    ],
    "links": [
      {
        "title": "After you apply",
        "url": "http://http://www.ed.ac.uk/studying/postgraduate/degrees/index.php?r=site/view&id=844",
        "supportText": "Find out more about what happens after you apply"
      },
      {
        "title": "Fees and Finance",
        "url": "http://www.ed.ac.uk/studying/postgraduate/fees-finance",
        "supportText": "Details of our tuition fees and additional costs associated with studying, as well as the scholarships and funding available to help you pursue your ambitions"
      },
      {
        "title": "Programme information",
        "url": "http://http://www.ed.ac.uk/studying/postgraduate/degrees/index.php?r=site/view&id=844",
        "supportText": "Learn more about your programme on the degree finder"
      },
      {
        "title": "Support for online learners",
        "url": "http://www.ed.ac.uk/studying/postgraduate/degree-guide/online-learning",
        "supportText": "Find out more about the support we offer online learners"
      },
      {
        "title": "Edinburgh University Students' Association",
        "url": "https://www.eusa.ed.ac.uk/",
        "supportText": "EUSA will help you get the most out of your time at the University"
      }
    ]
  },
  {
    "stage": "Applicant",
    "status": "We are processing your application",
    "programme": "Architectural Project Management (MSc)",
    "entryYear": "2016/7 entry",
    "workflow": [
      {
        "title": "Application Made",
        "status": "complete",
        "tooltip": "",
        "supportText": ""
      },
      {
        "title": "Application in process",
        "status": "process_action",
        "tooltip": "",
        "supportText": ""
      },
      {
        "title": "Offer",
        "status": "future",
        "tooltip": "",
        "supportText": ""
      }
    ],
    "tasks": [
      {
        "title": "Pay Application Fee",
        "url": "http://www.myed.ed.ac.uk",
        "supportText": "You will need to pay your application fee before we can continue processing your application. Please visit the Application Hub for more details on how to do this.",
        "status": ""
      },
      {
        "title": "Complete the fee status questionnaire",
        "url": "http://www.myed.ed.ac.uk",
        "supportText": "You will need to pay your application fee before we can continue processing your application. Please visit the Application Hub for more details on how to do this.",
        "status": ""
      },
      {
        "title": "Submit documents",
        "url": "http://www.myed.ed.ac.uk",
        "supportText": "",
        "status": "complete"
      }
    ],
    "information": [],
    "links": [
      {
        "title": "After you apply",
        "url": "http://http://www.ed.ac.uk/studying/postgraduate/degrees/index.php?r=site/view&id=844",
        "supportText": "Find out more about what happens after you apply"
      },
      {
        "title": "Fees and Finance",
        "url": "http://www.ed.ac.uk/studying/postgraduate/fees-finance",
        "supportText": "Details of our tuition fees and additional costs associated with studying, as well as the scholarships and funding available to help you pursue your ambitions"
      },
      {
        "title": "Programme information",
        "url": "http://http://www.ed.ac.uk/studying/postgraduate/degrees/index.php?r=site/view&id=844",
        "supportText": "Learn more about your programme on the degree finder"
      },
      {
        "title": "Support for online learners",
        "url": "http://www.ed.ac.uk/studying/postgraduate/degree-guide/online-learning",
        "supportText": "Find out more about the support we offer online learners"
      },
      {
        "title": "Edinburgh University Students' Association",
        "url": "https://www.eusa.ed.ac.uk/",
        "supportText": "EUSA will help you get the most out of your time at the University"
      }
    ]
  },
  {
    "stage": "Applicant",
    "status": "We are processing your application",
    "programme": "Intellectual Property Law (LLM)",
    "entryYear": "2016/7 entry",
    "workflow": [
      {
        "title": "Application Made",
        "status": "complete",
        "tooltip": "",
        "supportText": ""
      },
      {
        "title": "Application in process",
        "status": "process_action",
        "tooltip": "",
        "supportText": "You do not have any tasks at this time. Please be patient and check back regularly for updates."
      },
      {
        "title": "Offer",
        "status": "future",
        "tooltip": "",
        "supportText": ""
      }
    ],
    "tasks": [],
    "information": [],
    "links": []
  }
];
