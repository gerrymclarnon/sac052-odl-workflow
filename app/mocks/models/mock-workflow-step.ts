import {MockWorkflowStepTask} from "./mock-workflow-step-task";

export class MockWorkflowStep {
  timeline: string;
  task: MockWorkflowStepTask;
  data: string;
  hideNext: boolean;
}
