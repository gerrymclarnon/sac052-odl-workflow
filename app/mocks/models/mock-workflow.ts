import {MockWorkflowStep} from "./mock-workflow-step";

export class MockWorkflow {
  title: string;
  steps: MockWorkflowStep[];
}
