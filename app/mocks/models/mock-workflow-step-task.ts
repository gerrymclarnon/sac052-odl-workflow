export class MockWorkflowStepTask {
  title: string;
  description: string;
  instructions: string;
}
