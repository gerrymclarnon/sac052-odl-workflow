# Overview
Prototype for the SAC052 ODL Workflow project.

* [SAC052 project wiki](https://www.wiki.ed.ac.uk/display/OWP/SAC052%3A+ODL+Workflow+Project+Home)
* [Projects website](https://www.projects.ed.ac.uk/project/sac052)


# What's included
In addition to the main files for the project, a stripped down version of the [EdGEL distribution package](http://gel.ed.ac.uk/) is included here for convenience. The package includes Bootstrap and jQuery.

# Development Environment
Uses Angular2 (JavaScript library), HTML and CSS.

## Setup
Git and Node.js (v0.12.9) are required. Mac OSX users will probably find it easier installing [nvm](https://github.com/creationix/nvm) for Node Version management.
* [Git](https://git-scm.com/downloads)
* [Node.js](https://nodejs.org/en/#download) 

Clone the Git repo from the command line (this will create a sac052-odl-workflow sub-directory)

`git clone git@gitlab.com:gerrymclarnon/sac052-odl-workflow.git`

OR

`git clone https://gitlab.com/gerrymclarnon/sac052-odl-workflow.git`


Then use node to finish installing the required packages
* `cd sac052-odl-workflow`
* `npm install`

## Running the app
### Start/stop
From the command line:
* `npm start` to launch
* `Ctrl+C` to stop

While the app is running npm will take over that window. Open a new command line window to work with the repository while it is running.

### Pass JSON data via URL
Once the app has been launched, you need to specify a JSON data source to see the workflow. This can be done in the url using `?file=` with the filename. For example:

`http://localhost:3000/applications?file=/app/mocks/010-processing.json`

JSON data sets are stored in /app/mocks/

## Troubleshooting 
### 'Duplicate type error'
If after pulling the latest from Git you get an error relating to duplicate types then this is likely something to do with the typings directory.

The typings directory is created as part of running 'npm install' and is used during the Typescript to JavaScript transpilation. 
Sometimes after an update the typings directory needs to be deleted, and in some severe cases the node_modules directory also needs to be deleted, and 'npm install' run again.

## GitLab Pages

The GitLab Pages https://gerrymclarnon.gitlab.io/sac052-odl-workflow are deployed after a push to the repo using the .gitlab-ci.yml script.